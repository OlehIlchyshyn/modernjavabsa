package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private final String name;

	private final PositiveInteger powergrid;

	private final PositiveInteger capacitor;

	private PositiveInteger impactReductionPercent;

	private final PositiveInteger shieldRegeneration;

	private final PositiveInteger hullRegeneration;

	public DefenciveSubsystemImpl(String name, PositiveInteger powergrid, PositiveInteger capacitor,
			PositiveInteger impactReductionPercent, PositiveInteger shieldRegeneration,
			PositiveInteger hullRegeneration) {
		this.name = name;
		this.powergrid = powergrid;
		this.capacitor = capacitor;
		this.impactReductionPercent = impactReductionPercent;
		this.shieldRegeneration = shieldRegeneration;
		this.hullRegeneration = hullRegeneration;
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if (name == null || name.trim().length() == 0) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new DefenciveSubsystemImpl(name, powergridConsumption, capacitorConsumption, impactReductionPercent,
				shieldRegeneration, hullRegeneration);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergrid;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitor;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		if (this.impactReductionPercent.value() > 95) {
			this.impactReductionPercent = new PositiveInteger(95);
		}
		Integer newDamage = (int) Math
				.ceil((double) incomingDamage.damage.value() * ((100.0 - this.impactReductionPercent.value()) / 100.0));
		if (newDamage <= 0) {
			newDamage = 1;
		}
		return new AttackAction(new PositiveInteger(newDamage), incomingDamage.attacker, incomingDamage.target,
				incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegeneration, this.hullRegeneration);
	}

}
