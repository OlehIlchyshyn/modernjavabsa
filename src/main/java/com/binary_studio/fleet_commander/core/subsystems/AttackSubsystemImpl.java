package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private final String name;

	private final PositiveInteger powergrid;

	private final PositiveInteger capacitor;

	private final PositiveInteger optimalSpeed;

	private final PositiveInteger optimalSize;

	private final PositiveInteger baseDamage;

	public AttackSubsystemImpl(String name, PositiveInteger powergridRequirments, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
		this.name = name;
		this.powergrid = powergridRequirments;
		this.capacitor = capacitorConsumption;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name == null || name.trim().length() == 0) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergrid;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitor;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier;
		if (target.getSize().value() >= this.optimalSize.value()) {
			sizeReductionModifier = 1.0;
		}
		else {
			sizeReductionModifier = (double) target.getSize().value() / this.optimalSize.value();
		}
		double speedReductionModifier;
		if (target.getCurrentSpeed().value() <= this.optimalSpeed.value()) {
			speedReductionModifier = 1.0;
		}
		else {
			speedReductionModifier = (double) this.optimalSpeed.value() / (2.0 * target.getCurrentSpeed().value());
		}
		double damage = this.baseDamage.value() * Double.min(sizeReductionModifier, speedReductionModifier);
		int roundedDamage = (int) Math.ceil(damage);
		return new PositiveInteger(roundedDamage);
	}

	@Override
	public String getName() {
		return this.name;
	}

}
