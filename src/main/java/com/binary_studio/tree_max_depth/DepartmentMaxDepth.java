package com.binary_studio.tree_max_depth;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		return maxDepth(rootDepartment);
	}

	private static Integer maxDepth(Department department) {
		if (department == null) {
			return 0;
		}
		int maxdepth = 0;
		for (Department subdepartment : department.subDepartments) {
			maxdepth = Math.max(maxdepth, maxDepth(subdepartment));
		}
		return maxdepth + 1;
	}

}
